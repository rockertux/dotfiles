# start tmux sessions (IDE-like, etc)
work() { tmux source-file ~/tmux-sessions/"$@" }

_ldworksession() {
  compadd $(ls ~/tmux-sessions/)
}

_work() {
  _arguments -s "1:worksession:_ldworksession"
}

compdef _work work

# warsaw-docker
warsaw-docker() {
  xhost +;
  docker run -it --rm \
              -v /tmp/.X11-unix:/tmp/.X11-unix \
              -v $HOME/Downloads/Bank/$1:/home/bank/Downloads \
              --shm-size 2g \
              -e DISPLAY=unix$DISPLAY \
              --name warsaw-browser \
              lichti/warsaw-browser $1;
}

_warsaw-docker() {
  compadd -X "Bank codes" bb bbpj cef itau
}

compdef _warsaw-docker warsaw-docker

# load python virtualenvs
venv() { source ~/Workspace/virtualenvs/"$@"/bin/activate }

_ldvenv() {
  compadd $(ls ~/Workspace/virtualenvs/)
}

_venv() {
  _arguments -s "1:venv:_ldvenv"
}

compdef _venv venv
