call plug#begin()

" JS syntax highlight
Plug 'pangloss/vim-javascript'
" React JSX syntax highlight
Plug 'mxw/vim-jsx'
" TypeScript syntax highlight
Plug 'leafgarland/typescript-vim'
" Mocha helpers syntax highlight
Plug 'geekjuice/vim-mocha'

" Ruby & on Rails syntax highlight
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'

" C++ syntax highlight
Plug  'octol/vim-cpp-enhanced-highlight'

" Golang helpers
Plug 'fatih/vim-go',

" Kotlin tools
Plug 'udalov/kotlin-vim'

" Python IDE helper
Plug 'python-mode/python-mode', { 'branch': 'develop' }

" PostgreSQL-specific syntax highlight
Plug 'lifepillar/pgsql.vim'

" colorschemes collection
Plug 'flazz/vim-colorschemes'

" insert/remove comments in visual mode with gc
Plug 'tpope/vim-commentary'

call plug#end()

" Strip trailing spaces on save, keep cursor position
function! <SID>StripTrailingWhitespaces()
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  call cursor(l, c)
endfun

autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" Current line number and steps from current line
set number
set relativenumber

" Exit INSERT mode with jk
imap jk <ESC>

" Map tabs to 2 spaces
set expandtab
set tabstop=2
set shiftwidth=2

" Use 4-column hard tabs for go files
autocmd FileType go :setlocal sw=4 ts=4 noexpandtab

" Highlight column 80
set colorcolumn=80

" Display spaces, trailing spaces and control chars
set listchars=eol:$,tab:\|-,trail:=,space:·
set list

colorscheme 0x7A69_dark

hi SpecialKey ctermfg=69
hi NonText ctermfg=69
